package com.java.HW7.Work;

import java.util.Scanner;

public class EnumerateArithmetic {

	Scanner in = new Scanner(System.in);

	public void enumerateArithmetic() {

		System.out.println("Enter your first number: ");
		int a = in.nextInt();
		System.out.println("Enter your second number: ");
		int s = in.nextInt();
		System.out.println(
				"What operation do you want to preform? (A)ddition, (S)ubtraction, (M)ultiplication, (D)ivision");
		String d = in.next();

		if (d.equals("a")) {

			System.out.println("The sum of the numbers is " + (a + s));
		} else if (d.equals("s")) {

			System.out.println("The difference of the numbers is " + (a - s));
		} else if (d.equals("m")) {

			System.out.println("The product of the numbers is " + (a * s));
		} else if (d.equals("d")) {

			System.out.println("The quotient of the numbers is " + (a / s));
		}
	}

}
