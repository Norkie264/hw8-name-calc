package com.java.HW7;

import com.java.HW7.Work.Add;
import com.java.HW7.Work.EnumerateArithmetic;
import com.java.HW7.Work.James5x;
import com.java.HW7.Work.JamesNx;
import com.java.HW7.Work.UserNameN;

public class Main {

	public static void main(String args[]) {

		James5x a = new James5x();
		a.name();

		JamesNx b = new JamesNx();
		b.nameInputX();

		UserNameN c = new UserNameN();
		c.userNameTimes();

		Add d = new Add();
		d.numAdd();

		EnumerateArithmetic e = new EnumerateArithmetic();
		e.enumerateArithmetic();
	}
}
